@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        			<td colspan="8">
        				<a href="{{ route('prescriptions.create') }}" class="btn btn-success">Create</a>
        			</td>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Prescription</th>
        			<th>Start Date</th>
        			<th>Reorder Date</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($prescriptions as $key => $prescription)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $prescription->patient->firstname .' '. $prescription->patient->lastname }} </td>
							<td> {{ $prescription->start_at }} </td>
							<td> {{ $prescription->reorder_at }} </td>
							<td> {{ $prescription->instruction }} </td>
							<td>
								<center>

									<a href="{{ route('prescriptions.show', $prescription->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('prescriptions.edit', $prescription->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/prescriptions/'.$prescription->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="7"> {{ $prescriptions->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
			
		</div>
	</div>
</div>
@endsection