@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Create Prescription') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('prescriptions.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="drug_id" class="col-md-2 col-form-label text-md-right">{{ __('Prescription') }}</label>

                            <div class="col-md-6">
                                <select class="form-control{{ $errors->has('drug_id') ? ' is-invalid' : '' }}" name="drug_id" required autofocus>
                                    <option value="{{old('drug_id')}}">{{old('drug_id')}}</option>
                                    @foreach($drugs as $drug)
                                        <option value="{{$drug->id}}">{{ $drug->name}}</option>
                                    @endforeach
                                </select>
                                <input type="hidden" name="patient_id" value="{{ $patientId }}">
                                <input type="hidden" name="result_id" value="{{ $resultId }}">

                                @if ($errors->has('drug_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('drug_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="start_at" class="col-md-2 col-form-label text-md-right">{{ __('Start At') }}</label>

                            <div class="col-md-6">
                                <input id="start_at" type="date" class="form-control{{ $errors->has('start_at') ? ' is-invalid' : '' }}" name="start_at" value="{{ old('start_at') }}" required autofocus>

                                @if ($errors->has('start_at'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="reorder_at" class="col-md-2 col-form-label text-md-right">{{ __('Reorder At') }}</label>

                            <div class="col-md-6">
                                <input id="reorder_at" type="date" class="form-control{{ $errors->has('reorder_at') ? ' is-invalid' : '' }}" name="reorder_at" value="{{ old('reorder_at') }}" required autofocus>

                                @if ($errors->has('reorder_at'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('reorder_at') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="instruction" class="col-md-2 col-form-label text-md-right">{{ __('Instructions') }}</label>

                            <div class="col-md-6">
                                <textarea id="instruction" class="form-control{{ $errors->has('instruction') ? ' is-invalid' : '' }}" name="instruction" value="{{ old('instruction') }}" required autofocus>
                                    
                                </textarea>

                                @if ($errors->has('instruction'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('instruction') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
