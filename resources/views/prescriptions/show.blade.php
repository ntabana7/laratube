@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">

	        <div class="col-md-12">

	        	<div class="row">
        			<a href="{{ route('results.index') }}" class="btn btn-success">Back</a>
    			</div>
    			
    			<hr>

				<div class="row">

					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $result->patient->firstname .' '. $result->patient->lastname  }}</td>
						</tr>

						<tr>
							<td align="right">Result</td>
							<td>{{ $result->type->name }}</td>
						</tr>

						<tr>
							<td align="right">Doctor</td>
							<td>{{ $result->user->name }}</td>
						</tr>

						<tr>
							<td align="right">Doctor</td>
							<td>{{ $result->message }}</td>
						</tr>


						<tr>
							<td align="right">Created At</td>
							<td>{{ $result->created_at }}</td>
						</tr>
					</table>
					
				</div>

			</div>
	</div>
</div>
@endsection