@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">  	
        	<table class="table table-striped table-bordered">
        		<thead>
        			<tr>
        				<th colspan="8"><a href="{{ route('patients.create') }}" class="btn btn-success">Create</a></th>
        			</tr>
        		
        			<th>#</th>
        			<th>Firstname</th>
        			<th>Lastame</th>
                    <th>Phone</th>
        			<th>DOB</th>
        			<td colspan="3"><center><b>Action</b></center></td>
        		</thead>
        		<tbody>
        			@foreach($patients as $key => $patient)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $patient->firstname }} </td>
							<td> {{ $patient->lastname }} </td>
                            <td> {{ $patient->phone }} </td>
							<td> {{ date('Y-m-d' , strtotime($patient->dob)) }} </td>
							<td><a href="{{ route('patients.show', $patient->id) }}" class="btn btn-primary">View Records</a></td>
							<td><a href="{{ route('patients.edit', $patient->id) }}" class="btn btn-primary">Edit</a></td>
							<td>
									{!! Form::open(['url' => '/patients/'.$patient->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfoot>
        			<tr>
        				<td colspan="8">
        					{{ $patients->links() }}
        				</td>
        			</tr>
        		</tfoot>
			</table>
		</div>
	</div>
</div>
@endsection