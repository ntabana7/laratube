@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center" >
			
			<div class="row col-md-12">
	    			<a href="{{ route('patients.index') }}" class="btn btn-success">Back</a>
	    	</div>

    		<div class="col-md-12" id="profile">
		    		
	    		&nbsp;
		        <table class="table table-striped table-bordered">
		        	<tr>
		        		<td align="right"><b>Username</b>  </td>
		        		<td>{{ $patient->username }}</td>
		        	</tr>
		        	
		        	<tr>
		        		<td align="right"><b>Firstname</b></td>
		        		<td>{{ $patient->firstname }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Surname</b></td>
		        		<td>{{ $patient->lastname }}</td>
		        	</tr>
	    			
	    			<tr>
		        		<td align="right"><b>Email</b></td>
		        		<td>{{ $patient->email }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Phone</b></td>
		        		<td>{{ $patient->phone }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Gender</b></td>
		        		<td>{{ $patient->gender }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>DOB</b></td>
		        		<td>{{ $patient->dob }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Address</b></td>
		        		<td>{{ $patient->address }}</td>
		        	</tr>

		        </table>

	    	</div>

        	<div class="col-md-12 row">
        		<a href="{{route('results.create',['patientId' => $patient->id])}}">
        			<span class="btn btn-success" style="color:white">Add Result</span>
        		</a>
        	</div>

	    	<div class="col-md-12" id="result">
		    		
	    		&nbsp;
		        <table class="table table-striped table-bordered">
        		<thead>
        			<tr>
        				<th colspan="7"><center><h1>List of Results</h1></center></th>
        			</tr>
        			<tr>
	        			<th>#</th>
	        			<th>Type</th>
	        			<th>Doctor</th>
	        			<th>Message</th>
	        			<th colspan="3"><center><b>Action</b></center></th>
        			</tr>
        		</thead>
        		<tbody>
        			@foreach($patientResults as $key => $result)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $result->type->name }} </td>
							<td> {{ $result->user->name }} </td>
							<td> {{ $result->message }} </td>
							<td>
								<center>

									<a href="{{ route('results.show', $result->id) }}" class="btn btn-primary">
									View Prescriptions
								</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('results.edit', $result->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/results/'.$result->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfoot>
        			<tr>
        				<td colspan="7">
        					{{ $patientResults->links() }}
        				</td>
        			</tr>
        		</tfoot>
        	</table>
	    	</div>

	    	<div class="col-md-12" id="prescrition">		
	    		&nbsp;
			<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        	
        			<th  colspan="8">
        				<center><h1>All Prescriptions</h1></center>
        			</th>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Start At</th>
        			<th>Reorder At</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($patientPrescriptions as $key => $prescription)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $prescription->patient->firstname .' '. $prescription->patient->lastname }} </td>
							<td> {{ $prescription->start_at }} </td>
							<td> {{ $prescription->reorder_at }} </td>
							<td>
								<center>

									<a href="{{ route('prescriptions.show', $prescription->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('prescriptions.edit', $prescription->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/prescriptions/'.$prescription->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="7"> {{ $patientPrescriptions->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
	    	</div>


	    	<div class="col-md-12" id="appointment">
        	
        	<table class="table table-striped table-bordered">
        		<thead>
        			<tr>
        				<th colspan="8"><center><h1>List of Appointments</h1></center></th>
        			</tr>
        			<tr>
	        			<th>#</th>
	        			<th>Message</th>
	        			<th>Medication</th>
	        			<th>Symptom</th>
	        			<th>Appointment Date</th>
	        			<td colspan="3"><center><b>Action</b></center></td>
	        		</tr>
        		</thead>
        		<tbody>
        			@foreach($patientAppointments as $key => $appointment)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $appointment->message }} </td>
							<td> {{ $appointment->medication }} </td>
							<td> {{ $appointment->symptom->name }} </td>
							<td> {{ $appointment->appointment_date }} </td>
							<td>
								<center>

									<a href="{{ route('appointments.show', $appointment->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/appointments/'.$appointment->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfoot>
        			<tr>
        				<td colspan="8">
        					{{ $patientAppointments->links() }}
        				</td>
        			</tr>
        		</tfoot>
        	</table>
			</div>

@endsection