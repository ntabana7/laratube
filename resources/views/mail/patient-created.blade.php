@component('mail::message')
# {{$patient->firstname }} {{ $patient->lastname}}

This patient has been created successfully.

@component('mail::button', ['url' => ''])
View patient
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
