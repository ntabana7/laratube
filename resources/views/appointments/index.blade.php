@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="row">
        		<a href="{{ route('appointments.create') }}" class="btn btn-success">Create</a>
        	</div>

        	<hr>
        	
        	<table class="table table-striped table-bordered">
        		<thead>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Message</th>
        			<th>Medication</th>
        			<th>Symptom</th>
        			<th>Appointment Date</th>
        			<td colspan="3"><center><b>Action</b></center></td>
        		</thead>
        		<tbody>
        			@foreach($appointments as $key => $appointment)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td>
							<a href="{{ route('patients.show' , $appointment->patient)}}"> 
								{{ $appointment->patient->firstname  }} {{ $appointment->patient->lastname  }}
							</a>
							</td>
							<td> {{ $appointment->message }} </td>
							<td> {{ $appointment->medication }} </td>
							<td> {{ $appointment->symptom->name }} </td>
							<td> {{ $appointment->appointment_date }} </td>
							<td>
								<center>

									<a href="{{ route('appointments.show', $appointment->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/appointments/'.$appointment->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        	</table>
			
		</div>
	</div>
</div>
@endsection