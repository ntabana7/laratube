@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
			
			<div class="row col-md-12">
	    			<a href="{{ route('appointments.index') }}" class="btn btn-success">Back</a>
	    	</div>

    		<div class="row col-md-12">
		    		
	    		&nbsp;
		        <table class="table table-striped table-bordered">
		        	<tr>
		        		<td align="right"><b>Patient</b></td>
		        		<td>{{ $appointment->patient_id }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>Message</b>  </td>
		        		<td>{{ $appointment->message }}</td>
		        	</tr>
		        	
		        	<tr>
		        		<td align="right"><b>appointment_date</b></td>
		        		<td>{{ $appointment->appointment_date }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>medication</b></td>
		        		<td>{{ $appointment->medication }}</td>
		        	</tr>

		        	<tr>
		        		<td align="right"><b>symptom_id</b></td>
		        		<td>{{ $appointment->symptom_id }}</td>
		        	</tr>

		        </table>

	    	</div>

	    </div>

	</div>
@endsection