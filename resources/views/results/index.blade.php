@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        			<td colspan="8">
        				<a href="{{ route('results.create') }}" class="btn btn-success">Create</a>
        			</td>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Result</th>
        			<th>Doctor</th>
        			<th>Message</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($results as $key => $result)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $result->patient->firstname .' '. $result->patient->lastname }} </td>
							<td> {{ $result->type->name }} </td>
							<td> {{ $result->user->name }} </td>
							<td> {{ $result->message }} </td>
							<td>
								<center>

									<a href="{{ route('results.show', $result->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('results.edit', $result->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/results/'.$result->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="7"> {{ $results->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
			
		</div>
	</div>
</div>
@endsection