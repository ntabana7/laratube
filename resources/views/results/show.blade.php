@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">

	        <div class="col-md-12">

	        	<div class="row">
        			<a href="{{ route('results.index') }}" class="btn btn-success">Back</a>
    			</div>
    			
    			<hr>

				<div class="row">

					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $result->patient->firstname .' '. $result->patient->lastname  }}</td>
						</tr>

						<tr>
							<td align="right">Result</td>
							<td>{{ $result->type->name }}</td>
						</tr>

						<tr>
							<td align="right">Doctor</td>
							<td>{{ $result->user->name }}</td>
						</tr>

						<tr>
							<td align="right">Doctor</td>
							<td>{{ $result->message }}</td>
						</tr>


						<tr>
							<td align="right">Created At</td>
							<td>{{ $result->created_at }}</td>
						</tr>
					</table>
					
				</div>

        	<div class="row">
        		<a href="{{route('prescriptions.create',[
        				'patientId' => $result->patient_id,
        				'resultId'  => $result->id
        				])}}"><span class="btn btn-success" style="color:white">Add Prescription</span></a>
        	</div>

	    	<div class="row" id="prescrition">	
	    		&nbsp;
			<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        	
        			<th  colspan="8">
        				<center><h1>All Prescriptions</h1></center>
        			</th>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Patient</th>
        			<th>Start At</th>
        			<th>Reorder At</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($resultPrescriptions as $key => $prescription)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $prescription->patient->firstname .' '. $prescription->patient->lastname }} </td>
							<td> {{ $prescription->start_at }} </td>
							<td> {{ $prescription->reorder_at }} </td>
							<td>
								<center>

									<a href="{{ route('prescriptions.show', $prescription->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('prescriptions.edit', $prescription->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/prescriptions/'.$prescription->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="7"> {{ $resultPrescriptions->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
	    	</div>
</div>
@endsection