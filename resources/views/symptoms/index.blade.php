@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="row">
        		<a href="{{ route('symptoms.create') }}" class="btn btn-success">Create</a>
        	</div>

        	<hr>
        	
        	<table class="table table-striped table-bordered">
        		<thead>
        			<th>#</th>
        			<th>Name</th>
        			<td colspan="3"><center><b>Action</b></center></td>
        		</thead>
        		<tbody>
        			@foreach($symptoms as $key => $symptom)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $symptom->name }} </td>
							<td>
								<center>

									<a href="{{ route('symptoms.show', $symptom->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('symptoms.edit', $symptom->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/symptoms/'.$symptom->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
			
		</div>
	</div>
</div>
@endsection