@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">

	        <div class="col-md-12">

	        	<div class="row">
        			<a href="{{ route('drugs.index') }}" class="btn btn-success">Back</a>
    			</div>
    			
    			<hr>

				<div class="row">

					<table class="table table-striped table-bordered">
						<tr>
							<td align="right">Name</td>
							<td>{{ $drug->name }}</td>
						</tr>
					</table>
					
				</div>

			</div>
	</div>
</div>
@endsection