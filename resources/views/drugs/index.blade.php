@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<table class="table table-striped table-bordered table-condensed table-hover">
        		<thead>
        		<tr>
        			<td colspan="5">
        				<a href="{{ route('drugs.create') }}" class="btn btn-success">Create</a>
        			</td>
        		</tr>
        		<tr>
        			<th>#</th>
        			<th>Name</th>
        			<th colspan="3"><center><b>Action</b></center></th>
        		</tr>
        		</thead>
        		<tbody>
        			@foreach($drugs as $key => $drug)
						<tr> 
							<td> {{ $key + 1 }}</td>
							<td> {{ $drug->name }} </td>
							<td>
								<center>

									<a href="{{ route('drugs.show', $drug->id) }}" class="btn btn-primary">View</a>
															
								</center>
							</td>
							<td>
								<center>
									
									<a href="{{ route('drugs.edit', $drug->id) }}" class="btn btn-primary">Edit</a>	
															
								</center>
							</td>
							<td>
								<center>
									
									{!! Form::open(['url' => '/drugs/'.$drug->id,'onsubmit' =>'return confirm("Are you sure?")','method' => 'delete']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}	
								
								</center>
							</td>
						</tr>
					@endforeach
        		</tbody>
        		<tfooter>
        			<tr>
        				<td colspan="5"> {{ $drugs->links()}} </td>
        			</tr>
        		</tfooter>
        	</table>
			
		</div>
	</div>
</div>
@endsection