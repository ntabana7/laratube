<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

route::resource('drugs', 'DrugsController')->middleware('auth');
route::resource('types', 'TypesController')->middleware('auth');
route::resource('results', 'ResultsController')->middleware('auth');
route::resource('symptoms', 'SymptomsController')->middleware('auth');
route::resource('patients', 'PatientsController')->middleware('auth');
route::resource('appointments', 'AppointmentsController')->middleware('auth');
route::resource('prescriptions', 'PrescriptionsController')->middleware('auth');
