<?php

use Faker\Generator as Faker;

$factory->define(App\Prescription::class, function (Faker $faker) {
    return [
        'patient_id'  => 1,
        'drug_id'     => 1,
        'result_id'     => 1,
        'start_at' 	  => $faker->date($format = 'Y-m-d', $max = 'now'),
        'reorder_at'  => $faker->date($format = 'Y-m-d', $max = 'now'),
        'instruction' => $faker->paragraph
    ];
});
