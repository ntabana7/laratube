<?php

use Faker\Generator as Faker;

$factory->define(App\Appointment::class, function (Faker $faker) {
    return [
        'patient_id' 		=> 1,
        'symptom_id' 		=> 1,
        'medication' 		=> $faker->word.','.$faker->word,
        'message' 			=> $faker->paragraph,
        'appointment_date' 	=> $faker->date($format = 'Y-m-d', $max = 'now'), 
    ];
});
