<?php

use Faker\Generator as Faker;

$factory->define(App\Result::class, function (Faker $faker) {
    return [
        'user_id' => 1,//$faker->create(App\User::class)->id,
        'type_id' => 1,//$faker->create(App\Type::class)->id,
        'patient_id' => 1,//$faker->create(App\Patient::class)->id,
        'message'    => $faker->text
    ];
});
