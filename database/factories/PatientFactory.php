<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'username' 			=> $faker->name,
        'firstname' 		=> $faker->firstName,
        'lastname' 			=> $faker->lastName,
        'gender' 			=>  $faker->randomElement(['male', 'female']),
        'dob' 				=> $faker->date($format = 'Y-m-d', $max = 'now'),
        'address' 			=> $faker->address,
        'phone'             => $faker->unique()->phoneNumber,
        'email' 			=> $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' 			=> '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token'	=> str_random(10),
    ];
});
