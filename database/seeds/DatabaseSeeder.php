<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

	protected $toTruncate = ['drugs','types','results','patients','symptoms','appointments','prescriptions'];
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	
    	foreach ($this->toTruncate as $table) {
    		DB::table($table)->truncate();
    	}

        $this->call(DrugsTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(ResultsTableSeeder::class);
        $this->call(SymptomsTableSeeder::class);
        $this->call(PatientsTableSeeder::class);
        $this->call(PrescriptionsSeeder::class);
        $this->call(AppointmentsTableSeeder::class);
    }
}
