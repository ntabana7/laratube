<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

use App\Mail\PatientCreated;
use Mail;

class Patient extends Model
{
    use Notifiable;

    protected $guarded = [];

    public static function boot(){
    	
    	parent::boot();

    	static::created(function($patient) {
    		Mail::to('ntabanacoco@gmail.com')->send(
            	new PatientCreated($patient)
        	);
    	});
    }
    public function appointments() {

    	return $this->hasMany(Appointment::class);
    	
    }

    public function results() {

    	return $this->hasMany(Result::class);
    	
    }

    public function prescriptions() {

        return $this->hasMany(Prescription::class);
    }

}
