<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    protected $guarded = [];

    public function patient(){
    	return $this->belongsTo(Patient::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }

    public function type(){
    	return $this->belongsTo(Type::class);
    }

    public function prescriptions(){
        return $this->hasMany(Prescription::class);
    }
}
