<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription extends Model
{
    protected $guarded = [];

    public function patient() {
    	return $this->belongsTo(Patient::class);
    }

    public function drug(){
    	return $this->belongsTo(Drug::class);
    }
}
