<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Events\DrugCreated;
use Mail;

class Drug extends Model
{
    protected $guarded = [];

    protected $dispatchesEvents = [
        'created' => DrugCreated::class
    ];

    public function prescriptions() {
    	 return $this->hasMany(Prescription::class);
    }
}
