<?php

namespace App\Http\Controllers;

use App\Type;
use App\Result;
use App\Mail\ResultCreated;
use Illuminate\Http\Request;

class ResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('results.index')->withResults(auth()->user()->results()->paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        return view('results.create')
                    ->withPatientId($_GET['patientId'])
                    ->withTypes(Type::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validatedResult();

        $attributes['user_id'] = auth()->id();

        $result = Result::create($attributes);

        \Mail::to('ntabanacoco@gmail.com')->send(
            new ResultCreated($result)
        );

        return redirect()->route('patients.show', ['id' => $attributes['patient_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function show(Result $result)
    {
       return view('results.show')
       ->withResult($result)
       ->withResultPrescriptions($result->prescriptions()->paginate( 10 , ['*'] , '1page' ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function edit(Result $result)
    {
        return view('results.edit')->withResults($result);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Result $result)
    {
        $attributes = $this->validatedResult();

        $result->update($attributes);

        return redirect('results');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Result  $result
     * @return \Illuminate\Http\Response
     */
    public function destroy(Result $result)
    {
        $result->delete();
    }

    protected function validatedResult() {
        return request()->validate([
            'patient_id' => ['integer'],
            'type_id' => ['required', 'integer'],
            'message' => ['required', 'min:5']
        ]);
    }
}
