<?php

namespace App\Http\Controllers;

use App;
use App\Appointment;
use App\Notifications\AppointmentDue;
use Illuminate\Http\Request;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('appointments.index')->withAppointments(Appointment::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'symptom_id'        => ['required' , 'integer'],
            'medication'        => ['required' , 'min:3'],
            'message'           => ['required' , 'string','min:3'],
            'appointment_date'  => ['required' , 'date']
        ]);

        $attributes['patient_id'] = auth()->id();
        Appointment::create($attributes);

        $patient = App\Patient::first();
        $patient->notify(new AppointmentDue);

        return redirect('appointments');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        return view('appointments.show')->withAppointment($appointment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        return view('appointments.edit')->withAppointment($appointment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    { 
        $appointment->update(
            [
                'message'           => $request->message,
                'medication'        => $request->medication,
            ]);

        return redirect('/appointments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        $appointment->delete();

        return redirect('/appointments');
    }
}
