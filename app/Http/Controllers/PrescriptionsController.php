<?php

namespace App\Http\Controllers;

use App\Drug;
use App\Prescription;
use Illuminate\Http\Request;

class PrescriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('prescriptions.index')->withPrescriptions(Prescription::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('prescriptions.create')
                    ->withPatientId($_GET['patientId'])
                    ->withResultId($_GET['resultId'])
                    ->withDrugs(Drug::all());;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $this->validatedPrescription();

        $attributes['patient_id'] = $request->patient_id;
        $attributes['result_id']  = $request->result_id;
         

        Prescription::create($attributes);

        return redirect()->route('patients.show', ['id' => $attributes['patient_id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function show(Prescription $prescription)
    {
        return view('prescriptions.show')->withPrescription($prescription);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function edit(Prescription $prescription)
    {
        return view('prescriptions.edit')->withPrescription($prescription);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prescription $prescription)
    {
        $validatePrescription = $this->validatedPrescription();

        $prescription->update($validatePrescription);

        return redirect('/prescriptions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prescription  $prescription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Prescription $prescription)
    {
        $prescription->delete();

        return redirect()->route('patients.show', ['id' => $prescription->patient_id]);
    }

     protected function validatedPrescription() {
        return request()->validate([
            'drug_id'     => ['required' , 'integer'],
            'start_at'    => ['required', 'date'],
            'reorder_at'  => ['required', 'date'],
            'instruction' => ['required' , 'min:3']
        ]);
    }
}
