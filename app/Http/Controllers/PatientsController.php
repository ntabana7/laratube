<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;

class PatientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('patients.index')->withPatients(Patient::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patient = $this->validatedPatient();

        Patient::create($patient);

        return redirect('patients');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {

        return view('patients.show',[
            'patient' => $patient ,
            'patientResults' => $patient->results()->paginate( 10 , ['*'] , '1page' ),
            'patientPrescriptions' => $patient->prescriptions()->orderBy('id', 'DESC')->paginate( 4 , ['*'] , '2page'),
            'patientAppointments' => $patient->appointments()->orderBy('id', 'DESC')->paginate( 4 , ['*'] , '3page')
        ]);
                
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient)
    {
        return view('patients.edit')->withPatient($patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient)
    {
        $validataPatient = request()->validate([
            'firstname' => ['required', 'min:3'],
            'lastname'  => ['required' , 'min:3'],
            'gender'    => ['required' , 'min:3'],
            'address'   => ['required' , 'min:3'],
            'phone'     => ['required'],
            'dob'       => ['date']
        ]);

        $patient->update($validataPatient);

        return redirect('/patients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient)
    {
        $patient->delete();

        return redirect('/patients');
    }

    protected function validatedPatient() {
        return request()->validate([
            'username'  => ['required' , 'min:3'],
            'firstname' => ['required', 'min:3'],
            'lastname'  => ['required' , 'min:3'],
            'gender'    => ['required' , 'min:3'],
            'phone'     => ['required'],
            'address'   => ['required' , 'min:3'],
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:patients'],
            'password'  => ['required', 'string', 'min:6', 'confirmed'],

        ]);
    }
}
