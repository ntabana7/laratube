<?php

namespace App\Listeners;

use Mail;
use App\Events\DrugCreated;
use App\Mail\DrugCreated as DrugCreatedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDrugCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DrugCreated  $event
     * @return void
     */
    public function handle(DrugCreated $event)
    {
        Mail::to('ntabanacoco@gmail.com')->send(
            new DrugCreatedMail($event->drug)
       ); 
    }
}
